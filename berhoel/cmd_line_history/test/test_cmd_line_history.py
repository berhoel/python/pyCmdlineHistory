"""Tests for `cmd_line_history`."""

__date__ = "2024/08/05 23:48:08 hoel"
__author__ = "Berthold Höllmann"
__copyright__ = "Copyright © 2020, 2024 by Berthold Höllmann"
__credits__ = ["Berthold Höllmann"]
__maintainer__ = "Berthold Höllmann"
__email__ = "berhoel@gmail.com"
