..
  :Authors:
    - `Berthold Höllmann <berhoel@gmail.com>`__
  :Organization: Berthold Höllmann
  :Time-stamp: <2022-08-10 21:00:19 hoel>
  :Copyright: Copyright © 2022 by Berthold Höllmann

.. include:: ../README.rst

Example
=======

To show an example, I had input as below:

.. code-block:: console

  >>> i⭾
  id(          import       input(       is           issubclass(
  if           in           int(         isinstance(  iter(
  >>> import pwd⮐
  >>> print(f"{pwd.⭾
  pwd.getpwall()      pwd.getpwuid(
  pwd.getpwnam(       pwd.struct_passwd(
  >>> print(f"{pwd.__⭾
  pwd.__annotations__     pwd.__gt__(             pwd.__package__
  pwd.__class__(          pwd.__hash__()          pwd.__reduce__()
  pwd.__delattr__(        pwd.__init__(           pwd.__reduce_ex__(
  pwd.__dict__            pwd.__init_subclass__(  pwd.__repr__()
  pwd.__dir__(            pwd.__le__(             pwd.__setattr__(
  pwd.__doc__             pwd.__loader__()        pwd.__sizeof__()
  pwd.__eq__(             pwd.__lt__(             pwd.__spec__
  pwd.__format__(         pwd.__name__            pwd.__str__()
  pwd.__ge__(             pwd.__ne__(             pwd.__subclasshook__(
  pwd.__getattribute__(   pwd.__new__(
  >>> print(f"{pwd.__do⭾c__}")⮐
  ...
  >>> history⮐
  import pwd
  print(f"{pwd.__doc__}")

API documentation
=================

.. currentmodule:: berhoel

.. autosummary::
   :nosignatures:

   berhoel.cmd_line_history

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   _tmp/modules

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

..
  Local Variables:
  mode: rst
  compile-command: "make html"
  coding: utf-8
  End:
