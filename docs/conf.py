"""Configuration file for the Sphinx documentation builder."""

# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------

project = "pyCmdlineHistory"
copyright = """2009, Sunjoong LEE <sunjoong@gmail.com>;
© Copyright 2020, 2022, 2024 Berthold Höllmann  <berhoel@gmail.com>"""  # noqa:A001

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# -- Options for HTML output -------------------------------------------------

from berhoel.sphinx_settings import (  # noqa:E402;isort:skip
    ProjectTypes,
    setup,
    favicons,
    language,
    extensions,
    latex_engine,
    configuration,
    latex_elements,
    latex_show_urls,
    exclude_patterns,
    html_static_path,
    napoleon_use_ivar,
    napoleon_use_param,
    napoleon_use_rtype,
    sitemap_url_scheme,
    todo_include_todos,
    intersphinx_mapping,
    autodoc_default_options,
    napoleon_numpy_docstring,
    napoleon_google_docstring,
    napoleon_include_init_with_doc,
    napoleon_include_private_with_doc,
    napoleon_include_special_with_doc,
    napoleon_use_admonition_for_notes,
    napoleon_use_admonition_for_examples,
    napoleon_use_admonition_for_references,
)
from berhoel import sphinx_settings  # noqa:E402;isort:skip

globals().update(configuration().configuration())

extensions.extend(
    [
        "sphinx_argparse_cli",
    ]
)

html_theme = "berhoel_sphinx_theme"
html_theme_path = sphinx_settings.get_html_theme_path()

(  # noqa:B018
    ProjectTypes,
    setup,
    favicons,
    language,
    extensions,
    latex_engine,
    configuration,
    latex_elements,
    latex_show_urls,
    exclude_patterns,
    html_static_path,
    napoleon_use_ivar,
    napoleon_use_param,
    napoleon_use_rtype,
    sitemap_url_scheme,
    todo_include_todos,
    intersphinx_mapping,
    autodoc_default_options,
    napoleon_numpy_docstring,
    napoleon_google_docstring,
    napoleon_include_init_with_doc,
    napoleon_include_private_with_doc,
    napoleon_include_special_with_doc,
    napoleon_use_admonition_for_notes,
    napoleon_use_admonition_for_examples,
    napoleon_use_admonition_for_references,
)

# (Optional) Logo. Should be small enough to fit the navbar (ideally 24x24).
# Path should be relative to the ``_static`` files directory.
# h t ml_logo = "_static/my_logo.png"

html_static_path = ["_static"]

html_theme_options = {
    "navbar_links": [
        ("GitLab", "https://python.xn--hllmanns-n4a.de/pyCmdlineHistory/", True)
    ]
}
# If true, `todo` and `todoList` produce output, else they produce nothing.
todo_include_todos = True

# Set the value of html_baseurl in your Sphinx conf.py to the current
# base URL of your documentation.
html_baseurl = "https://berhoel.gitlab.io/python/pyCmdlineHistory/"
